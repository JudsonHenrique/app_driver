import 'package:app_driver/data/repository/user/user_repository_interface.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class UserChargingState {}

class HasUserChargingState implements UserChargingState {}

class NoUserChargingState implements UserChargingState {}

class ProcessingUserChargingState implements UserChargingState {}

class FailUserChargingState implements UserChargingState {}

class UserChargingCubit extends Cubit<UserChargingState> {
  final UserRepositoryInterface _repository;

  UserChargingCubit(this._repository) : super(ProcessingUserChargingState());

  Future<void> getCharging(String userId) async {
    emit(ProcessingUserChargingState());

    try {
      var response = await _repository.getCharging(userId);
      if (response.total != 0) {
        emit(HasUserChargingState());
      } else {
        emit(NoUserChargingState());
      }
    } catch (e) {
      emit(FailUserChargingState());
    }
  }
}
