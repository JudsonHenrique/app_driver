import 'package:app_driver/data/repository/user/user_repository_interface.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class UserPendencyState {}

class HasPendencyState implements UserPendencyState {}

class NoPendencyState implements UserPendencyState {}

class ProcessingUserPendencyState implements UserPendencyState {}

class FailUserPendencyState implements UserPendencyState {}

class UserPendencyCubit extends Cubit<UserPendencyState> {
  final UserRepositoryInterface _repository;

  UserPendencyCubit(this._repository) : super(ProcessingUserPendencyState());

  Future<void> listPendency(String userId) async {
    emit(ProcessingUserPendencyState());

    try {
      var response = await _repository.getPendency(userId);
      if (response.orders.isEmpty && response.boxes == 0) {
        emit(NoPendencyState());
      } else {
        emit(HasPendencyState());
      }
    } catch (e) {
      emit(FailUserPendencyState());
    }
  }
}
