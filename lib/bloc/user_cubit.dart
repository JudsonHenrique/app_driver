import 'package:app_driver/data/models/user_model.dart';
import 'package:app_driver/data/repository/user/user_repository_interface.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class UserState {}

class ProcessingUserState implements UserState {}

class SuccessUserState implements UserState {
  UserModel user;

  SuccessUserState(this.user);

  UserModel get props => user;
}

class FailUserState implements UserState {}

class UserCubit extends Cubit<UserState> {
  final UserRepositoryInterface _repository;

  UserCubit(this._repository) : super(ProcessingUserState());

  Future<void> user() async {
    emit(ProcessingUserState());

    try {
      var response = await _repository.getUser();
      emit(SuccessUserState(response!));
    } catch (e) {
      emit(FailUserState());
    }
  }
}
