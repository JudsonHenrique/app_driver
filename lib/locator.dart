import 'package:app_driver/data/repository/user/user_repository_interface.dart';
import 'package:get_it/get_it.dart';

import 'data/repository/service/http_service.dart';
import 'data/repository/user/user_repository.dart';

var getIt = GetIt.instance;

void setupLocator() {
  getIt.registerLazySingleton(() => HttpService());
  getIt.registerLazySingleton<UserRepositoryInterface>(
      () => UserRepository(getIt.get<HttpService>()));
}
