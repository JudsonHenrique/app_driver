import 'package:app_driver/pages/custom/custom_button.dart';
import 'package:flutter/material.dart';

class OptionsDriverComponent extends StatelessWidget {
  const OptionsDriverComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Center(
      child: Container(
        width: size.width * 0.9,
        height: size.height * 0.37,
        decoration: BoxDecoration(
            color: const Color(0xffF6F6F7),
            borderRadius: BorderRadius.circular(15)),
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'O que você deseja?',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: size.width * 0.045,
                  color: const Color(0xff323C50),
                ),
              ),
              // SizedBox(
              //   height: size.height * 0.005,
              // ),
              Text(
                'Selecione a opção abaixo',
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: size.width * 0.035,
                  color: const Color(0xff71767F),
                ),
              ),
              const CustomButton(
                label: 'Verificar carregamentos',
              ),
              const CustomButton(
                label: 'Entregar pedidos',
              ),
              const CustomButton(
                label: 'Consultar NFe',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
