import 'package:flutter/material.dart';

class DriveIdentificationComponent extends StatelessWidget {
  final String label;
  final String userId;
  const DriveIdentificationComponent(
      {Key? key, required this.label, required this.userId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Center(
        child: Container(
          width: size.width * 0.9,
          height: size.height * 0.22,
          decoration: BoxDecoration(
              color: const Color(0xffF6F6F7),
              borderRadius: BorderRadius.circular(15)),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 15, top: 15, right: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'N° de identificação:',
                      style: TextStyle(
                        // fontWeight: FontWeight.bold,
                        fontSize: size.width * 0.045,
                        color: const Color(0xff323C50),
                      ),
                    ),
                    SizedBox(
                      height: size.height * 0.01,
                    ),
                    Text(
                      label,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: size.width * 0.055,
                        color: const Color(0xff323C50),
                      ),
                    ),
                    SizedBox(
                      height: size.height * 0.02,
                    ),
                    Text(
                      'Utilize o QRCode \nsempre que for \nnecessário confirmar\nsua identidade',
                      style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: size.width * 0.030,
                        color: const Color(0xff71767F),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 150,
                height: 130,
                child: Image.network(
                    'https://southamerica-east1-facily-817c2.cloudfunctions.net/generate_qr_code?data=$userId'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
