import 'package:flutter/material.dart';

class ChargingComponent extends StatelessWidget {
  const ChargingComponent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Center(
      child: Container(
          width: size.width * 0.9,
          height: size.height * 0.12,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: const Color(0xffAAE6AE),
          ),
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Carregamento liberado',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: size.width * 0.045,
                    color: const Color(0xff355138),
                  ),
                ),
                Text(
                  'Dirija-se ao setor responsável e faça um novo carregamento',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: size.width * 0.040,
                    color: const Color(0xff355138),
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
