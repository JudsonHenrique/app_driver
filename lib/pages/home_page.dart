import 'package:app_driver/bloc/list_charging.dart';
import 'package:app_driver/bloc/list_pendency_cubit.dart';
import 'package:app_driver/bloc/user_cubit.dart';
import 'package:app_driver/data/repository/user/user_repository_interface.dart';
import 'package:app_driver/locator.dart';
import 'package:app_driver/pages/components/charging_component.dart';
import 'package:app_driver/pages/components/drive_identification_component.dart';
import 'package:app_driver/pages/components/options_driver_component.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomePage extends StatelessWidget {
  static const name = 'title-details-page';

  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<UserCubit>(
          create: (context) =>
              UserCubit(getIt.get<UserRepositoryInterface>())..user(),
        ),
        BlocProvider<UserPendencyCubit>(
            create: (context) =>
                UserPendencyCubit(getIt.get<UserRepositoryInterface>())),
        BlocProvider<UserChargingCubit>(
            create: (context) =>
                UserChargingCubit(getIt.get<UserRepositoryInterface>())),
      ],
      child: const HomePageView(),
    );
  }
}

class HomePageView extends StatelessWidget {
  const HomePageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: const Color(0xffFFFFFF),
      appBar: AppBar(
        backgroundColor: const Color(0xffE5E5E5),
        elevation: 0,
        actions: [
          SizedBox(
            width: size.width * 1,
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Icon(
                    FontAwesomeIcons.bars,
                    size: size.width * 0.05,
                    color: const Color(0xff323C50),
                  ),
                  Text(
                    'Facily Driver',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: size.width * 0.05,
                      color: const Color(0xff323C50),
                    ),
                  ),
                  IconButton(
                    icon: Icon(FontAwesomeIcons.sync,
                        size: size.width * 0.05,
                        color: const Color(0xff323C50)),
                    onPressed: () {
                      context.read<UserCubit>().user();
                    },
                  ),
                ],
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 15, top: 30),
              child:
                  BlocBuilder<UserCubit, UserState>(builder: (context, state) {
                if (state is ProcessingUserState) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                if (state is FailUserState) {
                  return const Center(
                    child: Text('Falha ao carregar os dados do usuário'),
                  );
                }
                if (state is SuccessUserState) {
                  context.read<UserPendencyCubit>().listPendency(state.user.id);
                  context.read<UserChargingCubit>().getCharging(state.user.id);

                  return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Olá ${state.user.firstName} ${state.user.lastName}!',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: size.width * 0.055,
                            color: const Color(0xff323C50),
                          ),
                        ),
                        SizedBox(
                          height: size.height * 0.01,
                        ),
                        Text(
                          'Tudo bem?',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: size.width * 0.045,
                            color: const Color(0xff323C50),
                          ),
                        ),
                        SizedBox(
                          height: size.height * 0.02,
                        ),
                        BlocBuilder<UserPendencyCubit, UserPendencyState>(
                            builder: (context, state) {
                          if (state is ProcessingUserPendencyState) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          if (state is FailUserPendencyState) {
                            return const Center(
                              child: Text(
                                  'Falha ao carregar as pendências do usuário'),
                            );
                          }
                          if (state is HasPendencyState) {
                            return const ChargingComponent();
                          }
                          return Container();
                        }),
                        SizedBox(
                          height: size.height * 0.02,
                        ),
                        BlocBuilder<UserChargingCubit, UserChargingState>(
                            builder: (context, state) {
                          if (state is ProcessingUserChargingState) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          if (state is FailUserChargingState) {
                            return const Center(
                              child: Text('Falha nos carregamentos do usuário'),
                            );
                          }
                          if (state is HasUserChargingState) {
                            return const OptionsDriverComponent();
                          }
                          return Container();
                        }),
                        SizedBox(
                          height: size.height * 0.02,
                        ),
                        DriveIdentificationComponent(
                            label: state.user.id, userId: state.user.id)
                      ]);
                }
                return Container();
              }),
            ),
          ],
        ),
      ),
    );
  }
}
