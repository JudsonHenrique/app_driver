import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final String label;
  const CustomButton({Key? key, required this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
        child: Container(
          width: size.width * 0.7,
          height: size.height * 0.07,
          decoration: BoxDecoration(
              color: const Color(0xff2B65F5),
              borderRadius: BorderRadius.circular(30)),
          child: Center(
            child: Text(
              label,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: size.width * 0.045,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
