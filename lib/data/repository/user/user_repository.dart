import 'package:app_driver/data/models/charging_model.dart';
import 'package:app_driver/data/models/pendency_model.dart';
import 'package:app_driver/data/models/user_model.dart';
import 'package:app_driver/data/repository/service/http_service.dart';
import 'package:app_driver/data/repository/user/user_repository_interface.dart';

class UserRepository implements UserRepositoryInterface {
  final HttpService _http;
  final _baseUrl =
      'https://hmlg-logistics-mobile-driver-cnbi5ucx.ue.gateway.dev/api/v1';
  UserModel? user;
  final tokenBearer =
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3d3dy5tZXJ5dG8uY29tLmJyLyIsImlhdCI6MTYzMzUzMjMwOSwibmJmIjoxNjMzNTMyMzA5LCJleHAiOjE2MzQxMzcxMDksImRhdGEiOnsidXNlciI6eyJpZCI6IjUwMCJ9fX0.cckvTtvCETxRSjqXKTWZeDNUDD-Ntc-nDVJ0H40x8dw';
  UserRepository(this._http);

  @override
  Future<UserModel?> getUser() async {
    if (user == null) {
      final token = tokenBearer;
      final uri = '$_baseUrl/users-me';
      final headers = {'Authorization': 'Bearer $token'};
      final response = await _http.getRequest(uri, headers: headers);
      user = UserModel.fromJson(response.content!['data']);
    }

    return user;
  }

  @override
  Future<PendencyModel> getPendency(userId) async {
    final uri = '$_baseUrl/driver-pendency?driver_id=$userId';
    final token = tokenBearer;
    final headers = {'Authorization': 'Bearer $token'};
    final response = await _http.getRequest(uri, headers: headers);

    if (response.success) {
      return PendencyModel.fromJson(response.content!);
    } else {
      throw Exception('error');
    }
  }

  @override
  Future<ChargingModel> getCharging(userId) async {
    final uri = '$_baseUrl/load-truck?driver_id=$userId';
    final token = tokenBearer;
    final headers = {'Authorization': 'Bearer $token'};
    final response = await _http.getRequest(uri, headers: headers);

    if (response.success) {
      return ChargingModel.fromJson(response.content!);
    } else {
      throw Exception('error');
    }
  }
}
