import 'package:app_driver/data/models/charging_model.dart';
import 'package:app_driver/data/models/pendency_model.dart';
import 'package:app_driver/data/models/user_model.dart';

abstract class UserRepositoryInterface {
  Future<UserModel?> getUser();
  Future<PendencyModel> getPendency(String userId);
  Future<ChargingModel> getCharging(String userId);
}
