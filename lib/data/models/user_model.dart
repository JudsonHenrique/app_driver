class UserModel {
  String id;
  String firstName;
  String lastName;

  UserModel({
    required this.id,
    required this.firstName,
    required this.lastName,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json['ID'] ?? '',
      firstName: json['first_name'],
      lastName: json['last_name'],
    );
  }
}
