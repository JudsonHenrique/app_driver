class PendencyModel {
  bool blocklisted;
  int boxes;
  List<int> orders;

  PendencyModel({
    required this.blocklisted,
    required this.boxes,
    required this.orders,
  });

  factory PendencyModel.fromJson(Map<String, dynamic> json) {
    return PendencyModel(
        blocklisted: json['blocklisted'],
        boxes: json['boxes'],
        orders: json['orders'].cast<int>());
  }
}
