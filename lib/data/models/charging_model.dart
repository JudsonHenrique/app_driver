class ChargingModel {
  int total;

  ChargingModel({
    required this.total,
  });

  factory ChargingModel.fromJson(Map<String, dynamic> json) {
    return ChargingModel(
      total: json['total'],
    );
  }
}
