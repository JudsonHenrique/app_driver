import 'package:app_driver/root_app.dart';
import 'package:flutter/material.dart';

import 'locator.dart';

void main() {
  setupLocator();
  runApp(const RootApp());
}
